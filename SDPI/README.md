# README #

* Automated system to help identify try to hack the system and compromise accounts
* Version 1.0.0
* git clone git@bitbucket.org:lnovoa/plexus-hb.git

### Technologies ###

* Spring boot 2.4.1 
* Java 8


### Configuration ###

* application.properties


### Access ###

* http://host:port/login



### Test ###

* HackerDetectorTest.java