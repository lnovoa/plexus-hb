package com.hotelbeds.supplierintegrations.hackertest.detector;

import com.hotelbeds.supplierintegrations.hackertest.detector.service.impl.HackerDetectorImpl;
import com.hotelbeds.supplierintegrations.hackertest.detector.util.CommonUtils;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HackerDetectorTest {

    @Autowired
    private HackerDetectorImpl hackerDetector;

    @Test
    public void testFiveAccessSuccess() throws InterruptedException {
        String access1 = hackerDetector.parseLine(CommonUtils.getRegister("user", "81.9.193.42", true));
        TimeUnit.MINUTES.sleep(1);

        String access2 = hackerDetector.parseLine(CommonUtils.getRegister("user", "81.9.193.42", true));
        TimeUnit.MINUTES.sleep(1);

        String access3 = hackerDetector.parseLine(CommonUtils.getRegister("user", "81.9.193.42", true));
        TimeUnit.MINUTES.sleep(1);

        String access4 = hackerDetector.parseLine(CommonUtils.getRegister("user", "81.9.193.42", true));
        TimeUnit.MINUTES.sleep(1);

        String access5 = hackerDetector.parseLine(CommonUtils.getRegister("user", "81.9.193.42", true));

        Assertions.assertAll(() -> Assertions.assertEquals(access1, null),
                () -> Assertions.assertEquals(access2, null),
                () -> Assertions.assertEquals(access3, null),
                () -> Assertions.assertEquals(access4, null),
                () -> Assertions.assertEquals(access5, "81.9.193.42"));

    }

    @Test
    public void testFiveAccessSuccess2() throws InterruptedException {
        String access1 = hackerDetector.parseLine(CommonUtils.getRegister("user", "81.9.193.42", true));
        TimeUnit.MINUTES.sleep(1);

        String access2 = hackerDetector.parseLine(CommonUtils.getRegister("user", "81.9.193.42", true));
        TimeUnit.MINUTES.sleep(1);

        String access3 = hackerDetector.parseLine(CommonUtils.getRegister("user", "81.9.193.42", true));
        TimeUnit.MINUTES.sleep(1);

        String access4 = hackerDetector.parseLine(CommonUtils.getRegister("user", "81.9.193.42", true));
        TimeUnit.MINUTES.sleep(3);

        String access5 = hackerDetector.parseLine(CommonUtils.getRegister("user", "81.9.193.42", true));

        Assertions.assertAll(() -> Assertions.assertEquals(access1, null),
                () -> Assertions.assertEquals(access2, null),
                () -> Assertions.assertEquals(access3, null),
                () -> Assertions.assertEquals(access4, null),
                () -> Assertions.assertEquals(access5, null));

    }

    @Test
    public void testAccessFailure() throws InterruptedException {
        String access1 = hackerDetector.parseLine(CommonUtils.getRegister("user", "81.9.193.42", false));

        String access2 = hackerDetector.parseLine(CommonUtils.getRegister("user2", "81.9.193.43", false));

        String access3 = hackerDetector.parseLine(CommonUtils.getRegister("user3", "81.9.193.44", false));

        Assertions.assertAll(() -> Assertions.assertEquals(access1, "81.9.193.42"),
                () -> Assertions.assertEquals(access2, "81.9.193.43"),
                () -> Assertions.assertEquals(access3, "81.9.193.44"));
    }

}
