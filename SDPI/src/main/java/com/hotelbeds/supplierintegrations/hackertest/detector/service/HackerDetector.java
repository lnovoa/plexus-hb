package com.hotelbeds.supplierintegrations.hackertest.detector.service;

public interface HackerDetector {

    String parseLine(String register);
}