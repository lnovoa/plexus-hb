package com.hotelbeds.supplierintegrations.hackertest.detector.securingweb;


import com.hotelbeds.supplierintegrations.hackertest.detector.service.HackerDetector;
import com.hotelbeds.supplierintegrations.hackertest.detector.util.CommonUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomAuthenticationSuccessHandler
        implements AuthenticationSuccessHandler {

    @Autowired
    private HackerDetector hackerDetector;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response, Authentication authentication)
            throws IOException {

        //Call to method for register access success
        hackerDetector.parseLine(CommonUtils.getRegister(authentication.getName(), request.getRemoteAddr(), true));

        //Redirect to home
        String redirectUrl = request.getContextPath() + "/home";
        response.sendRedirect(redirectUrl);
    }

}