package com.hotelbeds.supplierintegrations.hackertest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import java.io.IOException;

@Configuration
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class Loader {

    /**
     * Log file name and path.
     */

    public static void main(final String[] args) throws SecurityException, IOException {
        SpringApplication.run(Loader.class, args);
    }

}
