package com.hotelbeds.supplierintegrations.hackertest.detector.service.impl;

import com.hotelbeds.supplierintegrations.hackertest.detector.service.HackerDetector;
import com.hotelbeds.supplierintegrations.hackertest.detector.util.CommonUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service("hackerDetector")
public class HackerDetectorImpl implements HackerDetector {

    protected Log logger = LogFactory.getLog(this.getClass());

    public Map<String, List<Long>> mapInfo = new HashMap<>();


    /**
     * Detect and register line access by login.
     * Return remote host or null
     * @param register
     * @return String
     */
    @Override
    public String parseLine(String register) {

        logger.info("-------------- " + register + " --------------");

        // clean old registers
        cleanOldRegister(CommonUtils.getTimeMillis());

        if (!register.isEmpty()) {
            String[] array = register.split(",");

            //get host
            String host = array != null && array.length == 4 ? array[0] : null;
            //get date millis seconds
            long dateMillis = array != null && array.length == 4 ? Long.valueOf(array[1]) : null;

            // Add new register from login success
            if (array[2].equals(CommonUtils.SIGNIN_SUCCESS)) {
                newRegister(host, dateMillis);
            }

            //check if host is danger
            if (array[2].equals(CommonUtils.SIGNIN_FAILURE) || checkDangerHost(host, dateMillis)) {
                logger.info("****** DANGER IP! :: " + host);
                return host;
            }
        }

        return null;
    }

    /**
     * Add line register to map in memory
     * @param host
     * @param dateMillis
     */
    private void newRegister(String host, Long dateMillis) {
        if (mapInfo.get(host) != null) {
            List<Long> listAux = mapInfo.get(host);
            listAux.add(dateMillis);
            mapInfo.put(host, listAux);

        } else {
            List<Long> listAux = new ArrayList<>();
            listAux.add(dateMillis);
            mapInfo.put(host, listAux);
        }
    }

    /**
     * Calculate difference between two times and clean unnecessary registers
     * @param host
     * @param lastAccess
     ** @return
     */
    private boolean checkDangerHost(String host, Long lastAccess) {

        if (mapInfo.get(host) != null) {

            long minutes = timeControl(lastAccess, mapInfo.get(host).get(0));

            showTimes(mapInfo.get(host).get(0), lastAccess, minutes);

            if (mapInfo.get(host).size() >= CommonUtils.MINUTES && (minutes < CommonUtils.MINUTES)) {
                mapInfo.remove(host);

                return true;

            } else if (minutes >= CommonUtils.MINUTES) {
                mapInfo.remove(host);
            }
        }

        return false;
    }

    /**
     * Clean unnecessary registers for all registers
     * @param lastAccess
     */
    private void cleanOldRegister(long lastAccess) {
        // clean old register
        for (Map.Entry<String, List<Long>> entry : mapInfo.entrySet()) {

            if (timeControl(lastAccess, entry.getValue().get(entry.getValue().size() - 1)) >= CommonUtils.MINUTES) {
                mapInfo.remove(entry.getKey());
            }

        }
    }

    /**
     * Calculate minutes
     * @param time
     * @param time2
     * @return
     */
    private long timeControl(long time, long time2) {
        long minLastAccess = TimeUnit.MILLISECONDS.toMinutes(time);
        long minFirstAccess = TimeUnit.MILLISECONDS.toMinutes(time2);

        return (minLastAccess - minFirstAccess);
    }

    /**
     * Format times
     * @param firstAccess
     * @param lastAccess
     * @param minutes
     */
    private void showTimes(long firstAccess, long lastAccess, long minutes) {
        String formatted1 = CommonUtils.formatDate(firstAccess);
        String formatted2 = CommonUtils.formatDate(lastAccess);

        logger.info(formatted1 + " - " + formatted2 + ": " + minutes + " minutes");
    }


}