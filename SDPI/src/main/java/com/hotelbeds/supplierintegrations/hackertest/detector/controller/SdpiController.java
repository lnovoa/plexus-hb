package com.hotelbeds.supplierintegrations.hackertest.detector.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SdpiController {

    /**
     * Return to page home
     * @return
     */
    @GetMapping({"/home"})
    public String home() {
        return "home.html";
    }
}
