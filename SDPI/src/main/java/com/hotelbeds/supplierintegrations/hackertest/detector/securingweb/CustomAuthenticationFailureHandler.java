package com.hotelbeds.supplierintegrations.hackertest.detector.securingweb;


import com.hotelbeds.supplierintegrations.hackertest.detector.service.HackerDetector;
import com.hotelbeds.supplierintegrations.hackertest.detector.util.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomAuthenticationFailureHandler
        implements AuthenticationFailureHandler {

    @Autowired
    private HackerDetector hackerDetector;

    @Override
    public void onAuthenticationFailure(
            HttpServletRequest request,
            HttpServletResponse response,
            AuthenticationException exception)
            throws IOException {

        //Call to method for register access failure
        hackerDetector.parseLine(CommonUtils.getRegister((String) request.getParameter("username"),
                request.getRemoteAddr(), false));

        //redirect to login
        String redirectUrl = request.getContextPath() + "/login";
        response.sendRedirect(redirectUrl);
    }
}