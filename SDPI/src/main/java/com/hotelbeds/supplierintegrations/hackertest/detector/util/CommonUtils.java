package com.hotelbeds.supplierintegrations.hackertest.detector.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class CommonUtils {

    public static final String SIGNIN_SUCCESS = "SIGNIN_SUCCESS";
    public static final String SIGNIN_FAILURE = "SIGNIN_FAILURE";
    public static final Integer MINUTES = 5;
    public static final String PATTERN = "EEE, dd MMM yyyy HH:mm:ss zzz";

    public static String formatDate(Long time) {

        DateFormat dateFormat = new SimpleDateFormat(PATTERN);
        dateFormat.setTimeZone(TimeZone.getDefault());

        return dateFormat.format(new Date(time));
    }

    public static long getTimeMillis() {
        TimeZone timeZone = TimeZone.getDefault();
        Calendar calendar = Calendar.getInstance(timeZone);

        return calendar.getTimeInMillis();
    }

    public static String getRegister(String user, String host, boolean action) {

        return host
                .concat(",").concat(String.valueOf(getTimeMillis()))
                .concat(",").concat(!action ? SIGNIN_FAILURE : SIGNIN_SUCCESS)
                .concat(",").concat(user);
    }
}
